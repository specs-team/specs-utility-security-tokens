package org.specs.pkitokens.sts.utils;

import org.specs.specsdb.dao.UserDAO;
import org.specs.specsdb.model.User;
import org.specs.specsdb.utils.EMF;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

public class Authenticator {

    public User authenticate(String username, String password, HttpServletRequest request) throws
            Exception {

        EntityManager em = EMF.createEntityManager();
        try {
            UserDAO userDAO = new UserDAO(em);
            User user = userDAO.findByUsername(username);
            if (user == null) { // invalid username
                throw new InvalidCredentialsException("Invalid credentials.");
            } else {
                return user;
            }

        } finally {
            em.close();
        }
    }

    public static class InvalidCredentialsException extends Exception {
        public InvalidCredentialsException(String message) {
            super(message);
        }
    }
}
