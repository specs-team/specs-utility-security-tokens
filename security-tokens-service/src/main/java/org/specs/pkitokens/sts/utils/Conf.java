package org.specs.pkitokens.sts.utils;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Properties;

public class Conf {
    private static Conf instance = new Conf();
    private static final Logger logger = LogManager.getLogger(Conf.class);
    private static XMLConfiguration config;

    private Conf() {
    }

    public static void load(String configFile) throws Exception {
        try {
            config = new XMLConfiguration(configFile);
            logger.info(String.format("Configuration was loaded successfully from file '%s'.", configFile));
        } catch (Exception e) {
            throw new Exception(String.format("Failed to read configuration file '%s': %s", configFile,
                    e.getMessage()));
        }
    }

    public static String getSignerName() {
        return config.getString("signing.signerName");
    }

    public static String getSigningKeyStoreFile() {
        return config.getString("signing.signingKeyStore.keyStoreFile");
    }

    public static String getSigningKeyStorePass() throws Exception {
        return config.getString("signing.signingKeyStore.keyStorePass");
    }

    public static String getSigningCertFingerprint() {
        return config.getString("signing.signingKeyStore.signingCertFingerprint");
    }

    public static String getSigningPrivateKeyPass() {
        return config.getString("signing.signingKeyStore.signingPrivateKeyPass");
    }
}
